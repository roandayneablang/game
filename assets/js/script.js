const breathin = new Audio("assets/media/breathin-ariana-grande.mp3");
const fakelove = new Audio("assets/media/fake-love-bts.mp3");
let perfect = new Audio("assets/media/perfect-ed-sheeran.mp3");
let shallow = new Audio("assets/media/shallow-lady-gaga-bradley-cooper.mp3");
let toogood = new Audio("assets/media/too-good-at-goodbyes-sam-smith.mp3");
let withoutme = new Audio("assets/media/without-me-halsey.mp3");

const incorrect = new Audio("assets/media/incorrect.mp3");

let currentSong = null;
let currentSongIndex = null;

const songs = [
    {
        file: breathin,
        title: document.querySelector("#breathintitle"),
        singer: document.querySelector("#ariana")
    },
    {
        file: fakelove,
        title: document.querySelector("#fakelovetitle"),
        singer: document.querySelector("#bts")
    },
    {
        file: perfect,
        title: document.querySelector("#perfecttitle"),
        singer: document.querySelector("#sheeran")
    },
    {
        file: shallow,
        title: document.querySelector("#shallowtitle"),
        singer: document.querySelector("#gaga")
    },
    {
        file: toogood,
        title: document.querySelector("#toogoodtitle"),
        singer: document.querySelector("#sam")
    },
    {
        file: withoutme,
        title: document.querySelector("#withoutmetitle"),
        singer: document.querySelector("#halsey")
    }
]

const playBtn = document.querySelector("#btn");

// this gets activated when playBtn gets clicked
playBtn.addEventListener('click', function() {
    document.querySelector(".upper-container").style.backgroundImage = "none";

    if (currentSong !== null) {
        currentSongIndex = null
        pauseSong(currentSong);
    }

    let index = 0;
    shuffleArray(songs);
    resetSongList(songs);
    playSongFromList(index);
});

function playSongFromList(index) {
    if (index < songs.length) {
        currentSongIndex = index;
        playSong(songs[currentSongIndex].file);
    }
}

function playSong(songFile) {
    currentSong = songFile;
    currentSong.play();
}

function pauseSong(songFile) {
    currentSong = null;
    songFile.pause();
}

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 */
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

function resetSongList(songList) {
    for(let i=0; i < songList.length; i++) {
        songList[i].singer.style.visibility = "hidden";
        songList[i].title.style = "";

        songList[i].title.addEventListener('click', function() {
            if (currentSong === songList[i].file) {
                pauseSong(currentSong);
                songList[currentSongIndex].singer.style.visibility = "visible";
                songList[currentSongIndex].title.style.textDecoration = "line-through";
                currentSongIndex++;
                playSongFromList(currentSongIndex);
            } else if (currentSong === null) {
                // do nothing, kasi boplaks ka
            } else {
                incorrect.play();
            }
        });
    };
}